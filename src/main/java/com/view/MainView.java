package com.view;

import com.controller.BeanController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MainView {
    private Scanner scanner = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MainView.class);

    public void showMenu() {
        BeanController beanController = new BeanController();
        boolean flag = true;
        do {
            menu();
            switch (scanner.nextInt()) {
                case 1:
                    beanController.createBeans();
                    break;
                case 2:
                    flag = false;
            }
        } while (flag);


    }

    public void menu() {
        logger.info("\n1:showBeans\n2:Exit");
    }
}
