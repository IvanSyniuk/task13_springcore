package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource("bean.properties")
public class BeanC implements BeanValidator {

    private static Logger logger = LogManager.getLogger(BeanC.class);

    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private int value;

    @Override
    public void validate() {
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void init() {
        logger.info("Initializing bean - " + getClass().getName());
    }

    public void destroy() {
        logger.info("Destroying bean - " + getClass().getName());
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
