package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

    private static Logger logger = LogManager.getLogger(BeanA.class);

    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(BeanB beanB, BeanC beanC) {
        this.name = beanB.getName();
        this.value = beanC.getValue();
    }

    public BeanA(BeanB beanB, BeanD beanD) {
        this.name = beanB.getName();
        this.value = beanD.getValue();
    }

    public BeanA(BeanC beanC, BeanD beanD) {
        this.name = beanC.getName();
        this.value = beanD.getValue();
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public void validate() {

    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void destroy() throws Exception {
        logger.info("Bean destroyed" + getClass().getName());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Set value in " + getClass().getName());
    }
}
