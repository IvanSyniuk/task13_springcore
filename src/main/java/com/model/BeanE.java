package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {

    private static Logger logger = LogManager.getLogger(BeanE.class);

    private String name;
    private int value;

    public BeanE() {

    }

    public BeanE(BeanA beanA) {
        this.name = beanA.getName();
        this.value = beanA.getValue();
    }

    @Override
    public void validate() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.info("PostConstruct " + getClass().getName());
    }

    @PreDestroy
    public void preDestroy() {
        logger.info("PreDestroy " + getClass().getName());
    }
}
