package com.model;

public class BeanF implements BeanValidator {

    private String name;
    private int value;

    public BeanF() {

    }

    public BeanF(BeanD beanD, BeanB beanB) {
        this.name = beanD.getName();
        this.value = beanB.getValue();
    }

    @Override
    public void validate() {

    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
