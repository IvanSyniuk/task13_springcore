package com.configuraton;

import com.model.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("bean.properties")
@Import(SecondBeanConfiguration.class)
public class FirstBeanConfiguration {

    @Bean
    public BeanA createFromBC(BeanB getBeanB, BeanC getBeanC) {
        return new BeanA(getBeanB, getBeanC);
    }

    @Bean
    public BeanA createFromBD(@Qualifier("beanB") BeanB beanB, @Qualifier("beanD") BeanD beanD) {
        return new BeanA(beanB, beanD);
    }

    @Bean
    public BeanA createFromCD(@Qualifier("beanC") BeanC beanC, @Qualifier("beanD") BeanD beanD) {
        return new BeanA(beanC, beanD);
    }

    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn({"beanD", "beanB"})
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean
    public BeanE firstCreateBeanE(BeanA createFromBC) {
        return new BeanE(createFromBC);
    }

    @Bean
    public BeanE secondCreateBeanE(BeanA createFromBD) {
        return new BeanE(createFromBD);
    }

    @Bean
    public BeanE thirdCreateBeanE(BeanA createFromCD) {
        return new BeanE(createFromCD);
    }

    @Bean
    @Lazy
    public BeanF createFromDB(BeanD beanD, BeanB beanB) {
        return new BeanF(beanD, beanB);
    }
}
