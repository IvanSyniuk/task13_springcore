package com.controller;

import com.configuraton.FirstBeanConfiguration;
import com.model.BeanA;
import com.model.BeanF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BeanController {
    private static Logger logger = LogManager.getLogger(BeanController.class);

    public void createBeans() {
        AnnotationConfigApplicationContext configApplicationContext
                = new AnnotationConfigApplicationContext(FirstBeanConfiguration.class);

        BeanA fromBC = configApplicationContext.getBean("createFromBC", BeanA.class);
        BeanA fromBD = configApplicationContext.getBean("createFromBD", BeanA.class);
        BeanA fromCD = configApplicationContext.getBean("createFromCD", BeanA.class);
        BeanF beanF = configApplicationContext.getBean(BeanF.class);
        logger.info(fromBC.getName() + "  // BeanA1 //  " + fromBC.getValue());
        logger.info(fromBD.getName() + "  // BeanA2 //  " + fromBD.getValue());
        logger.info(fromCD.getName() + "  // BeanA3 //  " + fromCD.getValue());
        logger.info(beanF.getName() + "  // BeanF //  " + beanF.getValue());

    }
}
